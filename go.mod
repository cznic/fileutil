module modernc.org/fileutil

go 1.21

require modernc.org/mathutil v1.7.0

require github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
